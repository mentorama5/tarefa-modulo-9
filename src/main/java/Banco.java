import java.util.ArrayList;
import java.util.List;

public class Banco {
    //Tipos de contas(arraylist)
    private List<Conta> contas = new ArrayList<>();

    public void addConta(Conta conta) {
        this.contas.add(conta);
    }

    public List<Conta> getContas() {
        return contas;
    }

    //valor financeiro (somar o saldo de todas as contas)
    public double getSaldoTotal() {
        double soma=0;
        for(int i=0; i<contas.size(); i++){
            soma += contas.get(i).getSaldo();
        }
        return soma;
    }

    //Transferência entre contas (origem e destino)
    public void transferir(Conta origem, Conta destino, double valor){
        boolean resultado = origem.sacar(valor);
        if(resultado == true){
            destino.depositar(valor);
            System.out.println("Transferência realizada com sucesso.");
            System.out.println("O saldo da conta de origem é: R$"+ origem.getSaldo());
            System.out.println("O saldo da conta de destino é: R$"+ destino.getSaldo());
        }
        else {
            System.out.println("Saldo insuficiente para transferência.");
        }
    }
}
