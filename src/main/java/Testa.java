import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Testa {
    public static void main(String[] args) {
        Cliente cli1 = new Cliente("Karolina",true,"3333",10);
        Cliente cli2 = new Cliente("Michel",true,"xxxx",20);
        Cliente cli3 = new Cliente("Otávio",false,"planta",35);
        Cliente cli4 = new Cliente("Solineuza",false,"ssss",22);
        Cliente cli5 = new Cliente("Romeo",true,"rome",58);
        Cliente cli6 = new Cliente("João",true,"abcd",80);
        Cliente cli7 = new Cliente("Maria",true,"mmmm",81);
        Cliente cli8 = new Cliente("Bulma",true,"4321",7);
        Cliente cli9 = new Cliente("Benício",false,"bbbb",15);
        Cliente cli10 = new Cliente("Jove",true,"juma",90);

        List<Cliente> clientes = Arrays.asList(cli1,cli2,cli3,cli4,cli5,cli6,cli7,cli8,cli9,cli10);
        //clientes.forEach( cli -> System.out.println(cli.getCompras()));

        Stream<Cliente> stream = clientes.stream().filter(cliente -> cliente.getCompras() >= 20);

        List<Cliente> selecionados = stream.collect(Collectors.toList());
        selecionados.forEach(c -> System.out.println(c.getCompras()));

        MostraCliente mostra = new MostraCliente();

        mostra.imprimirClienteQueFezMaisCompras(clientes);
        mostra.imprimirClienteQueFezMenosCompras(clientes);
        mostra.imprimirMediaDecompras(clientes);

    }
}
