public class ContaSalario extends Conta {
    private int limiteDeSaques;
    private double taxaTributacao=0.01;

    public ContaSalario(int numero, int agencia, String banco, double saldo, int limiteDeSaques) {
        super(numero, agencia, banco, saldo);
        this.limiteDeSaques = limiteDeSaques;
    }

    @Override
    public double getSaldo() {
        return this.saldo;
    }

    public int getLimiteDeSaques() {
        return limiteDeSaques;
    }

    @Override
    public boolean sacar(double valor) {
        if(limiteDeSaques>0 && (getSaldo() >= valor)) {
            setSaldo(this.saldo - valor);
            limiteDeSaques = limiteDeSaques -1;
            System.out.println("Saque executado com sucesso");
            return true;
        }
        else {
            System.out.println("Limite de saque atingido ou valor não disponível.");
        }
        return false;
    }

    @Override
    public void tributar(){
        System.out.println("Conta salário possui taxa de tributação de 1% a.a");
        System.out.println("Exemplo: saldo de R$"+getSaldo());
        System.out.println("A taxa a ser descontada é de R$"+(getSaldo()*taxaTributacao));
    }
}
