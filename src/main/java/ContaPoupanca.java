public class ContaPoupanca extends Conta{
    private int diaAniversario;
    private double taxaDeJuros;
    private double taxaTributacao=1;

    public ContaPoupanca(int numero, int agencia, String banco, double saldo, int diaAniversario,
                         double taxaDeJuros) {
        super(numero, agencia, banco, saldo);
        this.diaAniversario = diaAniversario;
        this.taxaDeJuros = taxaDeJuros;
        }

    public double getSaldo(){
        return this.saldo;
    }

    @Override
    public boolean sacar(double valor) {
        if(getSaldo() >= valor) {
            setSaldo(this.saldo - valor);
            System.out.println("Saque executado com sucesso");
            return true;
        }
        else {
            System.out.println("Valor não disponível para saque.");
        }
        return false;
    }

    @Override
    public void tributar(){
        System.out.println("A conta poupança não possui taxa de tributação.");
    }

    /*@Override
    public double getSaldo() {
        if (this.diaAniversario >= 1) {
            this.saldo *= this.taxaDeJuros;
            return this.saldo;
        }
        else
            return this.saldo;
    }*/
}
