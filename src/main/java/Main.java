import java.util.Scanner;
public class Main {
    //Para utilizar o método de transferência é necessário criar 2 contas primeiramente.
    //A transferência será realizada entre essas duas contas. Conta 1 = origem e conta 2 = destino.

    public static void main(String[] args) {
        System.out.println("Menu de Opções Banco DoSul");
        int opcao;
        Banco banco = new Banco();
        do {
            System.out.println("1 - Criar nova conta");
            System.out.println("2 - Mostrar todas as contas do banco");
            System.out.println("3 - Mostrar saldo de todas as contas");
            System.out.println("4 - Realizar transferência entre contas");
            System.out.println("5 - Verificar taxa de imposto");
            System.out.println("6 - Sair");

            System.out.print("Digite a opção desejada: ");

            Scanner scanner = new Scanner(System.in);
            opcao = scanner.nextInt();

            processar(opcao, banco);
        }
        while (opcao != 6);
    }

    public static void processar(int opcao, Banco banco) {
        Scanner scanner = new Scanner(System.in);
        switch (opcao) {
            case 1: {
                System.out.println("Você escolheu a opção Criar nova conta");
                int opcaoConta;
                do {
                    System.out.println("1 - Criar conta corrente");
                    System.out.println("2 - Criar conta poupança");
                    System.out.println("3 - Criar conta salário");
                    System.out.println("4 - Voltar");

                    System.out.print("Digite a opção desejada: ");
                    opcaoConta = scanner.nextInt();

                    switch (opcaoConta) {

                        case 1: {
                            System.out.println("Você escolheu a opção Criar conta corrente");
                            System.out.println("Qual o saldo disponível?");
                            Double saldo = scanner.nextDouble();
                            banco.addConta(new ContaCorrente(1, 2,
                                    "DoSul", saldo, 500.00));
                            System.out.println("Conta corrente criada com sucesso.");
                            break;
                        }
                        case 2: {
                            System.out.println("Você escolheu a opção Criar conta poupança");
                            System.out.println("Qual o saldo disponível?");
                            Double saldo = scanner.nextDouble();
                            banco.addConta(new ContaPoupanca(2, 2,
                                    "DoSul", saldo, 10, 0.5));
                            System.out.println("Conta poupança criada com sucesso.");
                            break;
                        }
                        case 3: {
                            System.out.println("Você escolheu a opção Criar conta salário");
                            System.out.println("Qual o saldo disponível?");
                            Double saldo = scanner.nextDouble();
                            banco.addConta(new ContaSalario(3, 2,
                                    "DoSul", saldo, 2));
                            System.out.println("Conta salário criada com sucesso.");
                            break;
                        }
                        case 4: {
                            break;
                        }
                    }
                } while (opcaoConta != 4);
                break;
            }
            case 2: {
                System.out.println("Você escolheu a opção Mostrar todas as contas do banco");
                System.out.println(banco.getContas());
                break;
            }
            case 3: {
                System.out.println("Você escolheu a opção Mostrar saldo de todas as contas");
                System.out.println("O saldo total é R$" + banco.getSaldoTotal());
                break;
            }
            case 4: {
                System.out.print("Digite o valor a ser transferido para conta destino: R$");
                Double valor = scanner.nextDouble();
                banco.transferir(banco.getContas().get(0), banco.getContas().get(1), valor);
                break;
            }
            case 5: {
                System.out.println("Você escolheu a opção Verificar taxa de imposto");
                ContaCorrente contaC = new ContaCorrente(1, 2,
                        "DoSul", 1000, 500.00);
                ContaPoupanca contaP = new ContaPoupanca(2, 2,
                        "DoSul", 500, 10, 0.5);
                ContaSalario contaS = new ContaSalario(3, 2,
                        "DoSul", 600, 2);
                int opcaoConta;
                do {
                    System.out.println("1 - Verificar taxa da conta corrente");
                    System.out.println("2 - Verificar taxa da conta poupança");
                    System.out.println("3 - Verificar taxa da conta salário");
                    System.out.println("4 - Voltar");

                    System.out.println("Digite a opção desejada: ");
                    opcaoConta = scanner.nextInt();

                    switch (opcaoConta) {

                        case 1: {
                            contaC.tributar();
                            break;
                        }
                        case 2: {
                            contaP.tributar();
                            break;
                        }
                        case 3: {
                            contaS.tributar();
                            break;
                        }
                        case 4: {
                            break;
                        }
                    }
                } while (opcaoConta != 4);
                break;
            }
            case 6: {
                System.out.println("O programa foi encerrado.");
                break;
            }
        }
    }
}
