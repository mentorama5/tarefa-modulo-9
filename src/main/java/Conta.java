public abstract class Conta implements Tributacao {
    private int numero;
    private int agencia;
    private String banco;
    protected double saldo;

    public Conta(int numero, int agencia, String banco, double saldo) {
        this.numero = numero;
        this.agencia = agencia;
        this.banco = banco;
        this.saldo = saldo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getAgencia() {
        return agencia;
    }

    public void setAgencia(int agencia) {
        this.agencia = agencia;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public abstract double getSaldo();

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public void depositar(double valor) {
        setSaldo(this.saldo + valor);
    }

    public boolean sacar(double valor) {
        setSaldo(this.saldo - valor);
        return true;
    }

    /*public void transferir(Conta origem, Conta destino, double valor){
        boolean resultado = origem.sacar(valor);
        if(resultado == true){
            destino.depositar(valor);
            System.out.println("Transferência realizada com sucesso.");
            System.out.println("O saldo da conta de origem é: R$"+ origem.getSaldo());
            System.out.println("O saldo da conta de destino é: R$"+ destino.getSaldo());
        }
        else {
            System.out.println("Saldo insuficiente para transferência.");
        }
    }*/

    @Override
    public String toString() {
        return "Conta{" +
                "numero=" + numero +
                ", agencia=" + agencia +
                ", banco='" + banco + '\'' +
                ", saldo=" + saldo +
                '}';
    }

}
