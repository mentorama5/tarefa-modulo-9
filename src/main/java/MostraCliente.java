import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

public class MostraCliente implements Consumer {
    @Override
    public void accept(Object o) {
        System.out.println(o);
    }

    public void imprimirClienteQueFezMaisCompras(List<Cliente> clientes) {
        Cliente cliente = clientes.stream().max(Comparator.comparingInt(Cliente::getCompras)).get();
        System.out.println("Cliente que fez mais compras: " + cliente.getNome());
    }

    public void imprimirClienteQueFezMenosCompras(List<Cliente> clientes) {
        Cliente cliente = clientes.stream().min(Comparator.comparingInt(Cliente::getCompras)).get();
        System.out.println("Cliente que fez menos compras: " + cliente.getNome());
    }

    public void imprimirMediaDecompras (List<Cliente> clientes){
        System.out.println("A média de compras é: " +
                clientes.stream().mapToInt(Cliente::getCompras).average().getAsDouble());
    }
}
